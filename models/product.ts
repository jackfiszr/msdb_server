import { DataTypes, Model } from "../deps.ts";
import { DataSheet } from "./msds.ts";

export class Product extends Model {
  static table = "products";
  static timestamps = true;
  static fields = {
    materialIndexNumber: {
      ...DataTypes.string(15),
      primaryKey: true,
    },
    category: DataTypes.string(10), // airbags | batteries | chemicals | seatbelts | other
    // hierarchy: DataTypes.string(10),
    // componentsAmount: DataTypes.INTEGER,
    msds: DataTypes.string(25),
    // isHazardous: DataTypes.BOOLEAN,
    // needsHealthAndSafetyInfo: DataTypes.BOOLEAN,
    hasPolishLabel: DataTypes.BOOLEAN,
    hasDistributorInfo: DataTypes.BOOLEAN,
    lblAddedDate: DataTypes.DATE,
    lblWithdrawnDate: DataTypes.DATE,
    comment: DataTypes.string(100),
  };
  // static defaults = {
  //   componentsAmount: 1,
  // };
  static dataSheets() {
    return this.hasMany(DataSheet);
  }
}
