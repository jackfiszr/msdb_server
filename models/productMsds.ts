import { Relationships } from "../deps.ts";
import { Product } from "./product.ts";
import { DataSheet } from "./msds.ts";

export const ProductDataSheet = Relationships.manyToMany(Product, DataSheet);
