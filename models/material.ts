import { DataTypes, Model } from "../deps.ts";

export class Material extends Model {
  static table = "materials";
  static timestamps = true;
  static fields = {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    materialNumber: DataTypes.string(15),
    material: DataTypes.string(50),
    sLoc: DataTypes.string(5),
    followUpMaterial: DataTypes.string(15),
    totalStock: DataTypes.INTEGER,
    vendorAccountNumber: DataTypes.INTEGER,
    vendor: DataTypes.string(30),
    productHierarchy: DataTypes.string(15),
    exportedAt: DataTypes.string(10),
  };
}

export class ExcludedMaterial extends Model {
  static table = "excludedMaterials";
  static timestamps = true;
  static fields = {
    name: {
      ...DataTypes.string(50),
      primaryKey: true,
    },
  };
}
