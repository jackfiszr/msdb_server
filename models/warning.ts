import { DataTypes, Model } from "../deps.ts";

export class Warning extends Model {
  static table = "warnings";
  static timestamps = true;
  static fields = {
    code: {
      ...DataTypes.string(50),
      primaryKey: true,
    },
    text: DataTypes.string(500),
    type: DataTypes.string(10),
  };
}
