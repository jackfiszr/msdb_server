import { DataTypes, Model } from "../deps.ts";
import { Product } from "./product.ts";

export class DataSheet extends Model {
  static table = "dataSheets";
  static timestamps = true;
  static fields = {
    dataSheetId: {
      ...DataTypes.string(15),
      primaryKey: true,
    },
    ufi: DataTypes.string(20),
    author: DataTypes.string(25),
    actualizationDate: DataTypes.DATE,
    publicationDate: DataTypes.DATE,
    notificationDate: DataTypes.DATE,
    productName: DataTypes.string(100),
    originalName: DataTypes.string(100),
    productNumbers: DataTypes.string(500),
    // materialNumber: DataTypes.string(10),
    // substanceNumber: DataTypes.string(15),
    supplier: DataTypes.string(20),
    use: DataTypes.string(100),
    isHazardous: DataTypes.BOOLEAN,
    composition: DataTypes.string(1000),
    signalWord: DataTypes.string(20),
    contains: DataTypes.string(1000),
    pictograms: DataTypes.string(70),
    statements: DataTypes.string(200),
    addInfo: DataTypes.string(500),
    labelingRemarks: DataTypes.string(500),
    websiteId: DataTypes.INTEGER,
  };
  static defaults = {
    isHazardous: false,
  };
  static products() {
    return this.hasMany(Product);
  }
}
