import "../services/db.ts";
import { getMatDetailData } from "../services/et2000.ts";
import {
  createProduct,
  getProductDetails,
  updateProduct,
} from "../services/products.ts";

// const matDetail = await getMatDetailData();
// const matIndexes = Array.from(new Set(matDetail.map((el) => el.materialNumber)))
//   .sort();

// for (const matIndex of matIndexes) {
//   await createProduct({
//     materialIndexNumber: matIndex as string,
//     category: "",
//     msds: "",
//     hasPolishLabel: false,
//     hasDistributorInfo: false,
//     lblWithdrawnDate: null,
//     comment: "",
//   });
// }

// const accessories = matDetail.filter(el => el.productHierarchy.startsWith("ZH"))
// const accessoriesNumbers = Array.from(new Set(accessories.map((el) => el.materialNumber)))
//   .sort();

// console.log(accessoriesNumbers);

const prodsWithPL = [
  "8X0096151",
  "000096354L",
  "00A096327__020",
];

for (const materialIndexNumber of prodsWithPL) {
  await updateProduct({
    materialIndexNumber,
    hasPolishLabel: true,
    category: "chemicals",
  });
}

console.log("Done.");
