import "../services/db.ts";
import {
  getAllProductDataSheets,
  linkProductToDataSheet,
} from "../services/productSheets.ts";

await linkProductToDataSheet("G__A52579M2", "G__A52579");

const prdSds = await getAllProductDataSheets();

console.log(prdSds);
