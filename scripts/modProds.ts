import "../services/db.ts";
import { getProductsByCategory, updateProduct } from "../services/products.ts";

const chemicals = await getProductsByCategory("chemicals");

// console.log(chemicals);

for (const prod of chemicals) {
  const lblAddedDate = prod.createdAt.substring(0, 10);
  await updateProduct({
    ...prod,
    lblAddedDate,
  });
}
console.log("Done.");
