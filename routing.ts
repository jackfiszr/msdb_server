import { Router } from "./deps.ts";
import { getUser, serveStatic } from "./handlers/msdb.ts";

import {
  create as createProduct,
  destroy as deleteProduct,
  read as getProductData,
  readLbl as getLabelsData,
  update as updateProduct,
} from "./handlers/prod.ts";

import {
  create as createDataSheet,
  destroy as deleteDataSheet,
  getNonHazardousProducts,
  read as getDataSheets,
  update as updateDataSheet,
} from "./handlers/msds.ts";

import {
  addExcludedMaterial,
  delExcludedMaterial,
  getExcludedMaterials,
  getMaterialDetail,
  importMaterialDetail,
} from "./handlers/et2k.ts";

import {
  create as createWarning,
  destroy as deleteWarning,
  readAll as getWarnings,
} from "./handlers/warn.ts";

import getTableData from "./handlers/vgpl.ts";
import openLabelDir from "./handlers/labels.ts";

const router = new Router();
const apiRouter = new Router({ prefix: "/api" });

router.get<string>("/(.*)", serveStatic);

apiRouter
  .get<string>("/", (ctx) => {
    ctx.response.body = "API test";
  })
  .get<string>("/user", getUser)
  .get<string>("/vgp", getTableData)
  .get<string>("/prod/lbl", getLabelsData)
  .get<string>("/prod/:number?", getProductData)
  .post<string>("/prod/create", createProduct)
  .put<string>("/prod/update", updateProduct)
  .delete<string, { number: string }>("/prod/delete/:number", deleteProduct)
  .get<string>("/msds/nonhazardous", getNonHazardousProducts)
  .get<string>("/msds/:number?", getDataSheets)
  .post<string>("/msds/create", createDataSheet)
  .put<string>("/msds/update", updateDataSheet)
  .delete<string, { number: string }>("/msds/delete/:number", deleteDataSheet)
  .get<string>("/mat-detail/excluded", getExcludedMaterials)
  .get<string>("/mat-detail/:number?", getMaterialDetail)
  .post<string>("/mat-detail/excluded/add", addExcludedMaterial)
  .post<string>("/mat-detail/excluded/delete", delExcludedMaterial)
  .post<string>("/mat-detail/import", importMaterialDetail)
  .get<string, { dirname: string }>("/lbls/:dirname", openLabelDir)
  .get<string>("/warnings", getWarnings)
  .post<string>("/warnings/add", createWarning)
  .post<string>("/warnings/delete", deleteWarning);

export { apiRouter, router };
