import { helpers, RouterContext } from "../deps.ts";
import getTableData from "../services/vgpl.ts";

export default async (ctx: RouterContext<string>) => {
  const query = helpers.getQuery(ctx);
  ctx.response.body = await getTableData(query);
};
