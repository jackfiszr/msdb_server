import { ensureDirSync, join, open, RouterContext } from "../deps.ts";

export default async (
  { params, response }: RouterContext<string, { dirname: string }>,
) => {
  const dirname = `${params.dirname?.replace(/-[ABC]$/, "")}`;
  const dirpath = join("..", "NOWE", dirname);
  ensureDirSync(dirpath);
  await open(dirpath);
  response.status = 200;
};
