import { helpers, RouterContext } from "../deps.ts";
import {
  createDataSheet,
  deleteDataSheet,
  getDataSheetById,
  getDataSheetList,
  getDataSheets,
  getDataSheetsLike,
  nonHazardousProductsNumbersList,
  updateDataSheet,
} from "../services/dataSheets.ts";

export async function create({ request, response }: RouterContext<string>) {
  const data = await request.body().value;
  createDataSheet(data);
  response.status = 200;
}

export async function read(ctx: RouterContext<string>) {
  const query = helpers.getQuery(ctx);
  if (ctx?.params?.number) {
    const dataSheetId = ctx.params.number;
    ctx.response.body = await getDataSheetById(dataSheetId);
  } else if (query.id) {
    ctx.response.body = await getDataSheetsLike(query.id);
  } else if (Object.prototype.hasOwnProperty.call(query, "list")) {
    ctx.response.body = await getDataSheetList();
  } else {
    ctx.response.body = await getDataSheets();
  }
}

export async function update({ request, response }: RouterContext<string>) {
  const data = await request.body().value;
  updateDataSheet(data);
  response.status = 200;
}

export async function destroy(
  context: RouterContext<string, { number: string }>,
) {
  if (context?.params?.number) {
    const productNumber = context.params.number;
    const id = await deleteDataSheet(productNumber);
    context.response.body = { id };
    return;
  }
}

export async function getNonHazardousProducts(
  { response }: RouterContext<string>,
) {
  response.body = await nonHazardousProductsNumbersList();
}
