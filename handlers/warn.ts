import { RouterContext } from "../deps.ts";
import {
  addWarning,
  deleteWarning,
  getWarnings,
} from "../services/warnings.ts";

export async function readAll(ctx: RouterContext<string>) {
  ctx.response.body = await getWarnings();
}

export async function create(
  { request, response }: RouterContext<string>,
) {
  const warning = await request.body().value;
  addWarning(warning);
  response.status = 200;
}

export async function destroy(
  { request, response }: RouterContext<string>,
) {
  const warn = await request.body().value;
  deleteWarning(warn.code);
  response.status = 200;
}
