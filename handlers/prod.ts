import { helpers, RouterContext } from "../deps.ts";
import {
  createProduct,
  deleteProduct,
  getAllProductData,
  getLblProducts,
  getProductDetails,
  getProductList,
  getProductsByCategory,
  getProductsLike,
  updateProduct,
} from "../services/products.ts";

export async function create({ request, response }: RouterContext<string>) {
  const data = await request.body().value;
  createProduct(data);
  response.status = 200;
}

export async function read(ctx: RouterContext<string>) {
  const query = helpers.getQuery(ctx);
  if (ctx?.params?.number) {
    const productNumber = ctx.params.number;
    ctx.response.body = await getProductDetails(productNumber);
  } else if (query.id) {
    ctx.response.body = await getProductsLike(query.id);
  } else if (query.category) {
    ctx.response.body = await getProductsByCategory(query.category);
  } else if (Object.prototype.hasOwnProperty.call(query, "list")) {
    ctx.response.body = await getProductList();
  } else {
    ctx.response.body = await getAllProductData();
  }
}

export async function update({ request, response }: RouterContext<string>) {
  const data = await request.body().value;
  updateProduct(data);
  response.status = 200;
}

export async function destroy(
  context: RouterContext<string, { number: string }>,
) {
  if (context?.params?.number) {
    const productNumber = context.params.number;
    const id = await deleteProduct(productNumber);
    context.response.body = { id };
    return;
  }
}

export async function readLbl(
  { response }: RouterContext<string>,
) {
  response.body = await getLblProducts();
}
