import { RouterContext, send } from "../deps.ts";
import { STATIC_DIR, USERNAME } from "../config.ts";

export function getUser({ response }: RouterContext<string>) {
  response.body = USERNAME;
}

export async function serveStatic(ctx: RouterContext<string>) {
  await send(ctx, ctx.request.url.pathname, {
    root: `${Deno.cwd()}/${STATIC_DIR}`,
    index: "index.html",
  });
}
