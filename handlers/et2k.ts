import { helpers, RouterContext } from "../deps.ts";
import {
  addExcludedName,
  deleteExcludedName,
  getExcludedMaterialsList,
  getMatDetailForMatNumber,
  getMatDetailForMatNumbersLike,
  getMatDetailInStock,
  parseMaterialDetailFile,
  saveMatDetailToDatabase,
} from "../services/et2000.ts";

export async function getMaterialDetail(ctx: RouterContext<string>) {
  const query = helpers.getQuery(ctx);
  if (ctx?.params?.number) {
    const materialNumber = ctx.params.number;
    ctx.response.body = await getMatDetailForMatNumber(materialNumber);
  } else if (query.id) {
    ctx.response.body = await getMatDetailForMatNumbersLike(query.id);
  } else {
    ctx.response.body = await getMatDetailInStock();
  }
}

export async function getExcludedMaterials(ctx: RouterContext<string>) {
  ctx.response.body = await getExcludedMaterialsList();
}

export async function addExcludedMaterial(
  { request, response }: RouterContext<string>,
) {
  const name = await request.body().value;
  addExcludedName(name.name);
  response.status = 200;
}

export async function delExcludedMaterial(
  { request, response }: RouterContext<string>,
) {
  const name = await request.body().value;
  deleteExcludedName(name.name);
  response.status = 200;
}

export async function importMaterialDetail(
  { request, response }: RouterContext<string>,
) {
  const body = await request.body({ type: "form-data" });
  const form = await body.value.read();
  if (form.files) {
    const file = form.files[0].filename;
    if (file) {
      const matDetailObj = await parseMaterialDetailFile(file);
      await saveMatDetailToDatabase(matDetailObj);
      response.status = 200;
    }
  }
}
