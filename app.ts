import { Application, appVer } from "./deps.ts";
import { APP_HOST, APP_NAME, APP_PORT, USERNAME } from "./config.ts";
import { apiRouter, router } from "./routing.ts";
import { errorHandler } from "./middlewares/error.ts";
// import { logger, responseTime } from "./middlewares/logger.ts";
import { startApp } from "./utils.ts";
import "./services/db.ts";

const app = new Application();

app.use(errorHandler);
// app.use(logger);
// app.use(responseTime);

app.use(apiRouter.routes());
app.use(apiRouter.allowedMethods());
app.use(router.routes());
app.use(router.allowedMethods());

await startApp(app, {
  name: APP_NAME + " " + appVer,
  user: USERNAME,
  host: APP_HOST,
  port: APP_PORT,
  // open: false,
});
