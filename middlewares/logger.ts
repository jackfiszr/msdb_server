import { bold, Context, cyan, green } from "../deps.ts";

export async function logger(ctx: Context, next: () => Promise<void>) {
  await next();
  const rt = ctx.response.headers.get("X-Response-Time");
  console.log(
    `${green(ctx.request.method)} ${cyan(ctx.request.url.pathname)} - ${
      bold(String(rt))
    }`,
  );
}

export async function responseTime(ctx: Context, next: () => Promise<void>) {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${ms}ms`);
}
