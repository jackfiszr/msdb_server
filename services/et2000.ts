import { ExcludedMaterial, Material } from "../models/material.ts";
import { Product } from "../models/product.ts";
import { dlog } from "../utils.ts";

const COLUMNS = [
  { name: "Material", include: true },
  { name: "Material Number", include: true },
  { name: "Dv", include: false },
  { name: "SLoc", include: true },
  { name: "Unrestricted", include: false },
  { name: "BUn", include: false },
  { name: "Safety Stock", include: false },
  { name: "BUn2", include: false },
  { name: "Supersession typ", include: false },
  { name: "Follow-up material", include: true },
  { name: "Profit Ctr", include: false },
  { name: "PGr", include: false },
  { name: "ValCl", include: false },
  { name: "MovAvgPrice", include: false },
  { name: "Crcy", include: false },
  { name: "Total Stock", include: true },
  { name: "BUn3", include: false },
  { name: "Total Value", include: false },
  { name: "Crcy2", include: false },
  { name: "Vendor", include: true },
  { name: "R", include: false },
  { name: "Vendor Account Number", include: true },
  { name: "ItCGr", include: false },
  { name: "MPG", include: false },
  { name: "Product hierarchy", include: true },
  { name: "Exported at", include: true },
];

export type MatDetailEntry = {
  [key in typeof COLUMNS[number]["name"]]?: string;
};

const format = (productNumber: string) =>
  productNumber.replaceAll("-", "").replaceAll(" ", "_");

export async function parseMaterialDetailFile(filepath: string) {
  const matDetailArr = (await Deno.readTextFile(filepath))
    .trim()
    .split("\n")
    .map((str) => {
      const arr = str.trim().split("\t");
      arr.splice(1, 1);
      return arr.map((el) => el.trim());
    });
  const date = matDetailArr[0][0].substr(0, 10);

  const headers = [...matDetailArr[6]].map((el, i) => {
    return { en: COLUMNS[i]["name"], pl: el };
  }).filter((_el, index) => COLUMNS[index]["include"]);

  const matDetailData = matDetailArr.slice(8, matDetailArr.length - 2).map(
    (arr) => {
      const obj: MatDetailEntry = {};
      arr.forEach((el, index) => {
        if (COLUMNS[index]["include"]) {
          const propName = COLUMNS[index]["name"];
          if (["Material", "Follow-up material"].includes(propName)) {
            obj[propName] = format(el);
          } else {
            obj[propName] = el;
          }
        }
      });
      return obj;
    },
  );
  return {
    date,
    head: headers,
    body: matDetailData,
  };
}

export async function saveMatDetailToDatabase(matDetailObj: any) {
  for (const mat of matDetailObj.body) {
    const newMeterial = {
      // swapping matrial number/name because are swapped in source data
      materialNumber: mat["Material"],
      material: mat["Material Number"],
      sLoc: mat["SLoc"],
      followUpMaterial: mat["Follow-up material"],
      totalStock: mat["Total Stock"],
      // swapping vendor number/name because are swapped in source data
      vendorAccountNumber: mat["Vendor"],
      vendor: mat["Vendor Account Number"],
      productHierarchy: mat["Product hierarchy"],
      exportedAt: matDetailObj.date,
    };
    const oldMeterialsWithTheSameData = await Material
      .where("materialNumber", newMeterial.materialNumber)
      // .where("material", newMeterial.material)
      .where("sLoc", newMeterial.sLoc)
      // .where("followUpMaterial", newMeterial.followUpMaterial)
      // .where("totalStock", newMeterial.totalStock)
      .where("vendorAccountNumber", newMeterial.vendorAccountNumber)
      .where("vendor", newMeterial.vendor)
      // .where("productHierarchy", newMeterial.productHierarchy)
      // .where("exportedAt", newMeterial.exportedAt)
      .get() as any[];
    if (oldMeterialsWithTheSameData.length) {
      if (oldMeterialsWithTheSameData.length > 1) {
        throw new Error(
          `Duplicates in database:\n${oldMeterialsWithTheSameData}`,
        );
      }
      for (const oldMaterial of oldMeterialsWithTheSameData) {
        const updated = await Material.where("id", oldMaterial.id).update(
          newMeterial,
        );
        console.log(
          "UPDATE:",
          updated,
          oldMaterial.id,
          newMeterial.materialNumber,
        );
      }
    } else {
      const created = await Material.create(newMeterial);
      console.log("CREATE:", created, newMeterial.materialNumber);
      try {
        await Product.create({
          materialIndexNumber: newMeterial.materialNumber,
          category: "",
          msds: "",
          hasPolishLabel: false,
          hasDistributorInfo: false,
          lblAddedDate: null,
          lblWithdrawnDate: null,
          comment: "",
        });
      } catch (e) {
        if (e.code !== 19) throw new Error(e);
      }
    }
  }
  console.log("Done.");
}

export function getMatDetailData() {
  return Material.all();
}

export function getMatDetailInStock() {
  return Material.where("totalStock", ">", 0)
    .get();
}

export function getMatDetailForMatNumber(materialNumber: string) {
  return Material.where("materialNumber", materialNumber)
    .get();
}

export function getMatDetailForMatNumbersLike(materialNumber: string) {
  return Material.where("materialNumber", "like", `${materialNumber}%`)
    .get();
}

export function getExcludedMaterialsList() {
  return ExcludedMaterial.all();
}

export async function addExcludedName(name: string) {
  name = name.toUpperCase();
  if (await ExcludedMaterial.find(name)) {
    dlog({
      color: "red",
      title: "JUŻ ISTNIEJE",
      mainMsg: `"${name}"`,
    });
  } else {
    await ExcludedMaterial.create({ name });
    dlog({
      color: "green",
      title: "DODANO",
      mainMsg: `"${name}"`,
    });
  }
}

export async function deleteExcludedName(name: string) {
  await ExcludedMaterial.deleteById(name);
  dlog({
    color: "yellow",
    title: "USUNIĘTO",
    mainMsg: `"${name}"`,
  });
}
