import { Product } from "../models/product.ts";
import { Values } from "../deps.ts";
import { dlog, isValidIdNumber } from "../utils.ts";

export function getAllProductData() {
  return Product.all();
}

export function getProductDetails(materialIndexNumber: string) {
  return Product.where("materialIndexNumber", materialIndexNumber)
    .get();
}

export function getProductsLike(materialIndexNumber: string) {
  return Product.where("materialIndexNumber", "like", `${materialIndexNumber}%`)
    .get();
}

export function getProductsByCategory(category: string) {
  return Product.where("category", category)
    .get();
}

export function getLblProducts() {
  return Product.where("lblAddedDate", "is not", null).get();
}

export async function getProductList() {
  const productList = await Product.select("materialIndexNumber").all();
  return productList.map((prod) => prod.materialIndexNumber);
}

export async function createProduct(productData: Values) {
  productData.materialIndexNumber = `${productData.materialIndexNumber}`
    .toUpperCase();
  if (isValidIdNumber(`${productData.materialIndexNumber}`)) {
    if (await Product.find(productData.materialIndexNumber)) {
      dlog({
        color: "red",
        title: `${productData.materialIndexNumber}`,
        mainMsg: "ten numer produktu znajduje się już w bazie danych",
        // subMsg: collectionPath,
      });
    } else {
      await Product.create(productData);
      dlog({
        color: "green",
        title: `${productData.materialIndexNumber}`,
        mainMsg: "dodano produkt do bazy danych",
        // subMsg: collectionPath,
      });
    }
  }
}

export async function updateProduct(productData: Values) {
  productData.materialIndexNumber = `${productData.materialIndexNumber}`
    .toUpperCase();
  if (isValidIdNumber(`${productData.materialIndexNumber}`)) {
    await Product.where("materialIndexNumber", productData.materialIndexNumber)
      .update(
        productData,
      );
    dlog({
      color: "cyan",
      title: `${productData.materialIndexNumber}`,
      mainMsg: "zaktualizowano produkt",
      // subMsg: collectionPath,
    });
  }
}

export async function deleteProduct(productNumber: string) {
  productNumber = productNumber.toUpperCase();
  await Product.deleteById(productNumber);
  dlog({
    color: "magenta",
    title: productNumber,
    mainMsg: "usunięto produkt",
    // subMsg: collectionPath,
  });
}
