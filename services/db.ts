import { DB_PATH } from "../config.ts";
import { Database, SQLite3Connector } from "../deps.ts";
import { Product } from "../models/product.ts";
import { DataSheet } from "../models/msds.ts";
import { Warning } from "../models/warning.ts";
import { ProductDataSheet } from "../models/productMsds.ts";
import { ExcludedMaterial, Material } from "../models/material.ts";

const connector = new SQLite3Connector({
  filepath: DB_PATH,
});

export const db = new Database(connector);

db.link([
  ProductDataSheet,
  Product,
  DataSheet,
  Warning,
  Material,
  ExcludedMaterial,
]);

db.sync({ drop: false });
