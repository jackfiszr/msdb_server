import { DataSheet } from "../models/msds.ts";
import { Values } from "../deps.ts";
import { dlog, isValidIdNumber } from "../utils.ts";

export function getDataSheets() {
  return DataSheet.all();
}

export function getDataSheetById(dataSheetId: string) {
  return DataSheet.where("dataSheetId", dataSheetId).get();
}

export function getDataSheetsLike(dataSheetId: string) {
  return DataSheet.where("dataSheetId", "like", `${dataSheetId}%`).get();
}

export async function getDataSheetList() {
  return (await DataSheet.select("dataSheetId").all()).map((el) =>
    el.dataSheetId
  );
}

export async function createDataSheet(msds: Values) {
  msds.dataSheetId = `${msds.dataSheetId}`.toUpperCase();
  if (isValidIdNumber(`${msds.dataSheetId}`)) {
    if (await DataSheet.find(msds.dataSheetId)) {
      dlog({
        color: "red",
        title: `${msds.dataSheetId}`,
        mainMsg: "ten numer karty znajduje się już w bazie danych",
        // subMsg: collectionPath,
      });
    } else {
      await DataSheet.create(msds);
      dlog({
        color: "green",
        title: `${msds.dataSheetId}`,
        mainMsg: "dodano kartę do bazy danych",
        // subMsg: collectionPath,
      });
    }
  }
}

export async function updateDataSheet(msds: Values) {
  msds.dataSheetId = `${msds.dataSheetId}`.toUpperCase();
  if (isValidIdNumber(`${msds.dataSheetId}`)) {
    await DataSheet.where("dataSheetId", msds.dataSheetId).update(msds);
    dlog({
      color: "cyan",
      title: `${msds.dataSheetId}`,
      mainMsg: "zaktualizowano kartę",
      // subMsg: collectionPath,
    });
  }
}

export async function deleteDataSheet(dataSheetNumber: string) {
  dataSheetNumber = dataSheetNumber.toUpperCase();
  await DataSheet.deleteById(dataSheetNumber);
  dlog({
    color: "magenta",
    title: dataSheetNumber,
    mainMsg: "usunięto kartę",
    // subMsg: collectionPath,
  });
}

export async function nonHazardousProductsNumbersList() {
  return (await DataSheet.select("productNumbers").where("isHazardous", 0)
    .all()).map((el) =>
      `${el.productNumbers}`.split(";").map((el) => el.trim())
    ).flat();
}

export async function bulkUpdate(newData: Values[]) {
  for (const el of newData) {
    const msdsRecord = await DataSheet.find(el.dataSheetId) as Values;
    console.log(msdsRecord);
    msdsRecord.publicationDate = `${el.publicationDate}`.substring(0, 10);
    console.log(msdsRecord);
    await updateDataSheet(msdsRecord);
  }
  console.log("Done.");
}
