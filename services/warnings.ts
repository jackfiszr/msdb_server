import { Warning } from "../models/warning.ts";
import { Values } from "../deps.ts";
import { dlog } from "../utils.ts";

export function getWarnings() {
  return Warning.all();
}

export async function addWarning(warning: Values) {
  warning.code = `${warning.code}`.replaceAll(" ", "");
  if (await Warning.find(warning.code)) {
    dlog({
      color: "red",
      title: "JUŻ ISTNIEJE",
      mainMsg: `${warning.type} ${warning.code}`,
      subMsg: `${warning.text}`,
    });
  } else {
    await Warning.create(warning);
    dlog({
      color: "green",
      title: "DODANO",
      mainMsg: `${warning.type} ${warning.code}`,
      subMsg: `${warning.text}`,
    });
  }
}

export async function deleteWarning(code: string) {
  await Warning.deleteById(code);
  dlog({
    color: "yellow",
    title: "USUNIĘTO",
    mainMsg: `${code}`,
  });
}
