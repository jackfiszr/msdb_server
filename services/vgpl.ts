import { PROXY, PROXY_CONFIG } from "../config.ts";
import { DOMParser } from "../deps.ts";

type VgpTableEntry = {
  GRUPA: string;
  NAZWA: string;
  NUMER: string[];
  KARTA: string[];
};

async function vgpScrap() {
  const url =
    "https://www.vw-group.pl/volkswagen-group-polska,homologacja-i-ekologia.html";
  const data = [];
  let resp;
  try {
    if (PROXY) {
      const client = Deno.createHttpClient({
        proxy: PROXY_CONFIG,
      });
      resp = await fetch(url, { client });
    } else {
      resp = await fetch(url);
    }
    const html = await resp.text();
    const document = new DOMParser().parseFromString(html, "text/html");
    const msdsTable = document?.querySelector("#datatable_full");
    const tbody = msdsTable?.querySelector("tbody")?.querySelectorAll("tr");
    if (tbody) {
      for (const tr of tbody) {
        // @ts-ignore: error: TS2339 [ERROR]: Property 'querySelectorAll' does not exist on type 'Node'. ?????????
        const tds = tr.querySelectorAll("td");
        const record = {
          GRUPA: tds[0].textContent.trim(),
          NAZWA: tds[1].textContent.trim(),
          NUMER: tds[2].innerHTML.toString().replaceAll("'", "").replaceAll(
            "&nbsp;",
            "_",
          ).split(
            "<br>",
          ).map((e: string) => e.trim()),
          KARTA: [...tds[3].querySelectorAll("a")].map((el) =>
            el.getAttribute("href").split("/MSDS/")[1].split(".pdf")[0]
          ),
        };
        data.push(record);
      }
    }
  } catch (error) {
    console.error(error);
  }
  return data;
}

export default async (query: Record<string, string>) => {
  let result: VgpTableEntry[] | string[] = await vgpScrap();
  if (query.type) {
    result = result.filter((el: VgpTableEntry) =>
      el.GRUPA.toLowerCase().includes(query.type.toLowerCase())
    );
  }
  if (query.name) {
    result = result.filter((el: VgpTableEntry) =>
      el.NAZWA.toLowerCase().includes(query.name.toLowerCase())
    );
  }
  if (query.prod) {
    result = result.filter((el: VgpTableEntry) =>
      el.NUMER.includes(query.prod.toUpperCase())
    );
  }
  if (query.msds) {
    result = result.filter((el: VgpTableEntry) =>
      el.KARTA.includes(query.msds.toUpperCase())
    );
  }
  if (query.list) {
    switch (query.list) {
      case "type":
        result = [...new Set(result.map((el: VgpTableEntry) => el.GRUPA))]
          .sort();
        break;
      case "name":
        result = [...new Set(result.map((el: VgpTableEntry) => el.NAZWA))]
          .sort();
        break;
      case "prod":
        result = result.map((el: VgpTableEntry) => el.NUMER).flat().sort();
        break;
      case "msds":
        result = [
          ...new Set(result.map((el: VgpTableEntry) => el.KARTA).flat().sort()),
        ];
        break;
      case "dupl":
        result = result.map((el: VgpTableEntry) => el.NUMER).flat()
          .filter((el: string, index: number, products: string[]) => {
            if (products.indexOf(el) !== index) {
              return el;
            }
          });
        break;
    }
  }
  return result;
};
