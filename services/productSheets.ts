import { ProductDataSheet } from "../models/productMsds.ts";

export function getAllProductDataSheets() {
  return ProductDataSheet.all();
}

export async function linkProductToDataSheet(
  productId: string,
  datasheetId: string,
) {
  await ProductDataSheet.create([
    { productId, datasheetId },
  ]);
}
