import { dlog } from "./utils.ts";

export {
  dlog,
  startApp,
} from "https://bitbucket.org/jackfiszr/utils/raw/v1.3.4/mod.ts";

export function isValidIdNumber(idNumber: string) {
  if (idNumber.match(/^[A-Z0-9](?:[A-Z0-9_-]*[A-Z0-9])?$/)) {
    return true;
  } else {
    dlog({
      color: "red",
      title: idNumber,
      mainMsg:
        "numer identyfikacyjny produktu/karty musi być niepustym ciągiem znaków",
      subMsg: "dozwolone znaki to litery, liczby oraz podkreślnik (_)",
    });
  }
  return false;
}
