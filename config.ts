import { parse } from "./deps.ts";

const env = Deno.env.toObject();
const args = parse(Deno.args, { string: ["user"] });

export const USERNAME = args["user"] || env.USERNAME || "DEFAULT";
export const APP_NAME = "MSDB";
export const APP_HOST = env.APP_HOST || "localhost";
export const APP_PORT = env.APP_PORT || 5051;
export const DB_PATH = env.DB_PATH || "./db/msdb.sqlite";
export const PROXY = env.PROXY;
export const PROXY_CONFIG = {
  url: `http://${PROXY}`,
  // basicAuth: {
  //   username: USERNAME,
  //   password: "",
  // },
};
// if (PROXY) {
//   PROXY_CONFIG.basicAuth.password = await Secret.prompt(
//     "Podaj hasło do serwera proxy",
//   );
// }

export const STATIC_DIR = "dist/spa";
